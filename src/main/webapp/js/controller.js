function cloneObject (object)
{
    return Object.assign(Object.create(Object.getPrototypeOf(object)), object);
}

function webglCanvasOnMouseWheel_ChangingScale (e)
{
    e = window.event || e;
    var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
    var rect = ApplicationData.scene.webglCanvas.getBoundingClientRect();
    
    ApplicationData.scene.changeZoom(delta / 3.0, e.clientX - rect.left, e.clientY);
}

function getSeismogramHeaders (e)
{
    var rect = ApplicationData.scene.webglCanvas.getBoundingClientRect();
    var canvasXofCursor = (e.clientX - rect.left) * ApplicationData.ratio;
    var canvasYofCursor = ApplicationData.scene.webglCanvas.height + (rect.top - e.clientY) * ApplicationData.ratio;
    var globalXofCursor = Math.round(canvasXofCursor / ApplicationData.scene.scale - ApplicationData.scene.pivot.x);
    var globalYofCursor = canvasYofCursor / ApplicationData.scene.scale - ApplicationData.scene.pivot.y;
    var traces;
    var seismogramHeaders = {};
    
    seismogramHeaders.sequenceSeismogramNumber = "";
    seismogramHeaders.fieldSeismogramNumber = "";
    seismogramHeaders.shotPointNumber = "";
    seismogramHeaders.shotPoint = "";
    seismogramHeaders.depthPoint = "";
    seismogramHeaders.offsetPoint = "";
    seismogramHeaders.offsetPointDistance = "";
    
    if (ApplicationData.seismogram != null)
    {
        traces = ApplicationData.seismogram.webglTracesDrawView;
    }
    else
    {
        return seismogramHeaders;
    }
    
    var delta = 4 * ApplicationData.ratio / ApplicationData.scene.scale;
    var suspectTracesContainingCursor = [];
    var left = 0;
    var right = traces.length - 1;
    var mid;
    var bbox;
    var coordinates;
    var pointsCount;
    var flag;
    while (true)
    {
        mid = Math.floor((left + right) / 2);
        bbox = traces[mid].bbox;
        if (globalXofCursor <= bbox.x + bbox.width + delta && globalXofCursor >= bbox.x - delta)
        {
            break;
        }
        if (left >= right)
        {
            break;
        }
        if (globalXofCursor > bbox.x + bbox.width)
        {
            left = mid + 1;
        }
        else
        {
            right = mid - 1;
        }
    }
    
    if (mid == undefined)
    {
        alert("Что-то пошло не так");
        return seismogramHeaders;
    }
    
    suspectTracesContainingCursor.push(mid);
    
    var index = mid;
    left = 0;
    right = traces.length - 1;
    while (true)
    {
        index--;
        if (index < left)
        {
            break;
        }
        bbox = traces[index].bbox;
        if (globalXofCursor <= bbox.x + bbox.width + delta && globalXofCursor >= bbox.x - delta)
        {
            suspectTracesContainingCursor.push(index);
        }
        else
        {
            break;
        }
    }
    index = mid;
    while (true)
    {
        index++;
        if (index > right)
        {
            break;
        }
        bbox = traces[index].bbox;
        if (globalXofCursor <= bbox.x + bbox.width + delta && globalXofCursor >= bbox.x - delta)
        {
            suspectTracesContainingCursor.push(index);
        }
        else
        {
            break;
        }
    }
    
    suspectTracesContainingCursor.sort(function (a, b) {
        if (a > b)
        {
            return 1;
        }
        if (a < b)
        {
            return -1;
        }
    });
    
    
    for (var i = 0; i < suspectTracesContainingCursor.length; i++)
    {
        index = suspectTracesContainingCursor[i];
        coordinates = traces[index].points;
        pointsCount = coordinates.length / 2;
        left = 1;
        right = pointsCount - 1;
        flag = false;
        while (true)
        {
            mid = Math.floor((left + right) / 2);
            if (coordinates[2 * mid + 3] <= globalYofCursor && globalYofCursor < coordinates[2 * mid + 1])
            {
                flag = true;
                break;
            }
            if (left >= right)
            {
                break;
            }
            if (globalYofCursor < coordinates[2 * mid + 3])
            {
                left = mid + 1;
            }
            else if (globalYofCursor >= coordinates[2 * mid + 1])
            {
                right = mid - 1;
            }
        }
        if (!flag)
        {
            return seismogramHeaders;
        }
        
        var x1 = coordinates[2 * mid];
        var y1 = coordinates[2 * mid + 1];
        var x2 = coordinates[2 * mid + 2];
        var y2 = coordinates[2 * mid + 3];
        //var x = (globalYofCursor - y1) * (x2 - x1) / (y2 - y1) + x1;
        
        if (Math.abs((globalYofCursor - y1) * (x2 - x1) / (y2 - y1) + x1 - globalXofCursor) <= delta)
        {
            seismogramHeaders.sequenceSeismogramNumber = traces[suspectTracesContainingCursor[i]]["sequenceSeismogramNumber"];
            seismogramHeaders.fieldSeismogramNumber = traces[suspectTracesContainingCursor[i]]["fieldSeismogramNumber"];
            seismogramHeaders.shotPointNumber = traces[suspectTracesContainingCursor[i]]["shotPointNumber"];
            seismogramHeaders.shotPoint = traces[suspectTracesContainingCursor[i]]["shotPoint"];
            seismogramHeaders.depthPoint = traces[suspectTracesContainingCursor[i]]["depthPoint"];
            seismogramHeaders.offsetPoint = traces[suspectTracesContainingCursor[i]]["offsetPoint"];
            seismogramHeaders.offsetPointDistance = traces[suspectTracesContainingCursor[i]]["offsetPointDistance"];
            
            return seismogramHeaders;
        }
    }
    
    return seismogramHeaders;
}

function webglCanvasOnMouseMove_DraggingScene (e)
{
    if (e.buttons == 1 && e.target == ApplicationData.scene.webglCanvas)
    {
        ApplicationData.scene.drag(e.movementX, e.movementY);
    }
    else if (e.buttons == 0 && e.target == ApplicationData.scene.webglCanvas)
    {
        var seismogramHeaders = getSeismogramHeaders(e);
        var tableHeaderValueRow = document.getElementById("seis-information-table").rows[1];
        tableHeaderValueRow.cells[0].textContent = seismogramHeaders.sequenceSeismogramNumber;
        tableHeaderValueRow.cells[1].textContent = seismogramHeaders.fieldSeismogramNumber;
        tableHeaderValueRow.cells[2].textContent = seismogramHeaders.shotPointNumber;
        tableHeaderValueRow.cells[3].textContent = seismogramHeaders.shotPoint;
        tableHeaderValueRow.cells[4].textContent = seismogramHeaders.depthPoint;
        tableHeaderValueRow.cells[5].textContent = seismogramHeaders.offsetPoint;
        tableHeaderValueRow.cells[6].textContent = seismogramHeaders.offsetPointDistance;
    }
}

function onWindowResize (e)
{
    updateWebglCanvasSize();
    ApplicationData.scene.updateAxes();
    ApplicationData.scene.render();
}

function updateWebglCanvasSize ()
{
    var canvas = document.getElementById("webgl-canvas");
    var axesCanvas = document.getElementById("axes-canvas");
    var pictureDiv = document.getElementById("picture-div");
    var canvasMarginLeft = canvas.getBoundingClientRect().left - axesCanvas.getBoundingClientRect().left;
    var canvasMarginTop = canvas.getBoundingClientRect().top - axesCanvas.getBoundingClientRect().top;
    axesCanvas.style.width = pictureDiv.clientWidth + "px";
    axesCanvas.style.height = pictureDiv.clientHeight + "px";
    canvas.style.width = pictureDiv.clientWidth - canvasMarginLeft + "px";
    canvas.style.height = pictureDiv.clientHeight - canvasMarginTop + "px";
    axesCanvas.width = pictureDiv.clientWidth * ApplicationData.ratio;
    axesCanvas.height = pictureDiv.clientHeight * ApplicationData.ratio;
    canvas.width = (pictureDiv.clientWidth - canvasMarginLeft) * ApplicationData.ratio;
    canvas.height = (pictureDiv.clientHeight - canvasMarginTop) * ApplicationData.ratio;
    ApplicationData.scene.reshape();
}

function addSeismogramButtonOkOnClick (button)
{
    var files = button.form.elements["seismic_file"].files;
    var seismogramName = button.form.elements["seismogram_name"].value;
    var profileName = button.form.elements["profile_name"].value;
    var jsTree = ApplicationData.jstree;
    var selectedNodes = jsTree.getSelectedNodes();
    
    if (files.length == 0)
    {
        alert("Выберите файл");
        return false;
    }
    
    if (profileName == "")
    {
        alert("Введите имя профиля");
        return false;
    }
    
    if (seismogramName == "")
    {
        alert("Введите имя сейсмограммы");
        return false;
    }
    
    if (selectedNodes.length == 0)
    {
        alert("Выберите директорию для сохранения");
        return false;
    }
    if (selectedNodes.length != 1)
    {
        alert("Выберите одну директорию для сохранения");
        jsTree.deselectAllNodes();
        return false;
    }
    
    var selectedNode = selectedNodes[0];
    
    if (jsTree.getNodeType(selectedNode) != JsTree.NODE_TYPE_DIRECTORY)
    {
        alert("Выберите директорию для сохранения");
        return false;
    }
    
    var jstreeDirectoryId = selectedNode["li_attr"]["id"];
    
    ServerConnector.addSeismogram(button.form, jstreeDirectoryId);
}

function addDirectoryButtonOkOnClick (button)
{
    if (!checkIfDirectorySelected(button.form))
    {
        return;
    }
    
    var jsTree = ApplicationData.jstree;
    var selectedNode = jsTree.getSelectedNodes()[0];
    
    if (selectedNode != undefined)
    {
        var jstreeParentDirectoryId = selectedNode["li_attr"]["id"];
    }
    
    ServerConnector.addDirectory(button.form, jstreeParentDirectoryId);
}

function checkIfDirectorySelected (form)
{
    var directoryName = form.elements["directory_name"].value;
    var jsTree = ApplicationData.jstree;
    var selectedNodes = jsTree.getSelectedNodes();
    
    if (directoryName == "")
    {
        alert("Введите имя директории");
        return false;
    }
    
    if (selectedNodes.length != 1 && selectedNodes.length != 0)
    {
        alert("Выберите одну родительскую папку");
        jsTree.deselectAllNodes();
        return false;
    }
    
    var selectedNode = selectedNodes[0];
    
    if (selectedNodes.length == 1 && jsTree.getNodeType(selectedNode) != JsTree.NODE_TYPE_DIRECTORY)
    {
        alert("Выберите папку для сохранения");
        return false;
    }
    
    return true;
}

function checkIfSeismogramSelected ()
{
    var jsTree = ApplicationData.jstree;
    var selectedNodes = jsTree.getSelectedNodes();
    
    if (selectedNodes.length == 0)
    {
        alert("Выберите сейсмограму");
        return false;
    }
    
    if (selectedNodes.length != 1)
    {
        alert("Выберите один файл");
        jsTree.deselectAllNodes();
        return false;
    }
    
    var selectedNode = selectedNodes[0];
    
    if (jsTree.getNodeType(selectedNode) != JsTree.NODE_TYPE_BASE_SEISMOGRAM &&
            jsTree.getNodeType(selectedNode) != JsTree.NODE_TYPE_FILTERED_SEISMOGRAM &&
            jsTree.getNodeType(selectedNode) != JsTree.NODE_TYPE_SUM_SEISMOGRAM)
    {
        alert("Выберите сейсмограму");
        jsTree.deselectAllNodes();
        return false;
    }
    return true;
}

function drawSelectedSeismogram (partNumber)
{
    if (!checkIfSeismogramSelected())
    {
        return;
    }
    
    var jsTree = ApplicationData.jstree;
    var sortingTraceField = Seismogram.currentSortingTraceField;
    var selectedNode = jsTree.getSelectedNodes()[0];
    
    $("#tree-button").text(jsTree.getPath(selectedNode));
    
    var seismogramId = jsTree.getNodeId(selectedNode);
    
    ServerConnector.getTracesFromServer(seismogramId, sortingTraceField, partNumber)
            .then(function (traceObjects) {
                traceObjects = JSON.parse(traceObjects);
                var seismogram = new Seismogram(seismogramId, traceObjects);
                
                if (ApplicationData.seismogram != null && ApplicationData.seismogram.id == seismogram.id
                        && ApplicationData.seismogram.tMin != null && ApplicationData.seismogram.tMax)
                {
                    seismogram.tMin = ApplicationData.seismogram.tMin;
                    seismogram.tMax = ApplicationData.seismogram.tMax;
                    seismogram.cutTime();
                }
                
                ApplicationData.seismogram = seismogram;
                ApplicationData.seismogram.draw();
            });
}

function drawFullSeismogramButtonOnClick ()
{
    Seismogram.currentPartNumber = null;
    drawSelectedSeismogram(Seismogram.currentPartNumber);
}

function drawPartialSeismogramButtonOnClick ()
{
    if (Seismogram.currentPartNumber == null)
    {
        Seismogram.currentPartNumber = 0;
    }
    
    drawSelectedSeismogram(Seismogram.currentPartNumber);
}

function increasePartNumberButtonOnClick ()
{
    if (Seismogram.currentPartNumber == null)
    {
        Seismogram.currentPartNumber = 0;
    }
    Seismogram.currentPartNumber++;
    drawPartialSeismogramButtonOnClick();
}

function decreasePartNumberButtonOnClick ()
{
    if (Seismogram.currentPartNumber == null)
    {
        Seismogram.currentPartNumber = 0;
    }
    Seismogram.currentPartNumber--;
    drawPartialSeismogramButtonOnClick();
}

function addFilterButtonOnClick ()
{
    var filterClassName = $("#filter-select").find("option:selected").val();
    ApplicationData.filters.push(eval("new " + filterClassName + "()"));
}

function partialApplyFiltersButtonOnClick ()
{
    if (!checkIfSeismogramSelected())
    {
        return;
    }
    
    var jsTree = ApplicationData.jstree;
    var selectedNode = jsTree.getSelectedNodes()[0];
    
    $("#tree-button").text(jsTree.getPath(selectedNode));
    
    var seismogramId = jsTree.getNodeId(selectedNode);
    
    if (Seismogram.currentPartNumber == null)
    {
        Seismogram.currentPartNumber = 0;
    }
    
    ApplicationData.filters.apply(seismogramId, Seismogram.currentPartNumber);
}

function fullApplyFiltersButtonOnClick ()
{
    if (!checkIfSeismogramSelected())
    {
        return;
    }
    
    var jsTree = ApplicationData.jstree;
    var selectedNode = jsTree.getSelectedNodes()[0];
    
    $("#tree-button").text(jsTree.getPath(selectedNode));
    
    var seismogramId = jsTree.getNodeId(selectedNode);
    
    Seismogram.currentPartNumber = null;
    
    ApplicationData.filters.apply(seismogramId, Seismogram.currentPartNumber);
}


function sortBySNTdOnClick ()
{
    ApplicationData.seismogram.sort("sequenceSeismogramNumber");
    ApplicationData.seismogram.draw();
}

function sortByFNTdOnClick ()
{
    ApplicationData.seismogram.sort("fieldSeismogramNumber");
    ApplicationData.seismogram.draw();
}

function sortBySPNTdOnClick ()
{
    ApplicationData.seismogram.sort("shotPointNumber");
    ApplicationData.seismogram.draw();
}

function sortBySPTdOnClick ()
{
    ApplicationData.seismogram.sort("shotPoint");
    ApplicationData.seismogram.draw();
}

function sortByDPTdOnClick ()
{
    ApplicationData.seismogram.sort("depthPoint");
    ApplicationData.seismogram.draw();
}

function sortByOPTdOnClick ()
{
    ApplicationData.seismogram.sort("offsetPoint");
    ApplicationData.seismogram.draw();
}

function sortByOPDTdOnClick ()
{
    ApplicationData.seismogram.sort("offsetPointDistance");
    ApplicationData.seismogram.draw();
}

function onScaleXInputNumberChange ()
{
    var newScaleX = parseFloat(this.value);
    if (Seismogram.userScaleX != newScaleX)
    {
        Seismogram.userScaleX = newScaleX;
        var seismogram = ApplicationData.seismogram;
        if (seismogram != null)
        {
            seismogram.updateDrawView();
            seismogram.draw();
        }
    }
}

function onScaleTInputNumberChange ()
{
    var newScaleT = parseFloat(this.value);
    if (Seismogram.userScaleT != newScaleT)
    {
        Seismogram.userScaleT = newScaleT;
        var seismogram = ApplicationData.seismogram;
        if (seismogram != null)
        {
            seismogram.updateDrawView();
            seismogram.draw();
        }
    }
}

function onTraceIndentScaleInputNumberChange ()
{
    var newTraceIndentScale = parseFloat(this.value);
    if (Seismogram.userTraceIndentScale != newTraceIndentScale)
    {
        Seismogram.userTraceIndentScale = newTraceIndentScale;
        var seismogram = ApplicationData.seismogram;
        if (seismogram != null)
        {
            seismogram.updateDrawView();
            seismogram.draw();
        }
    }
}

function onTMinInputNumberChange ()
{
    var newTMin = parseInt(this.value) / 1000;
    var seismogram = ApplicationData.seismogram;
    if (seismogram != null && ApplicationData.seismogram.tMin != newTMin)
    {
        seismogram.tMin = newTMin;
        seismogram.cutTime();
        seismogram.draw();
    }
}

function onTMaxInputNumberChange ()
{
    var newTMax = parseInt(this.value) / 1000;
    var seismogram = ApplicationData.seismogram;
    if (seismogram != null && ApplicationData.seismogram.tMax != newTMax)
    {
        seismogram.tMax = newTMax;
        seismogram.cutTime();
        seismogram.draw();
    }
}

function onTraceMaxCountInputNumberChange ()
{
    var newTracMaxCount = parseInt(this.value);
    if (Seismogram.traceMaxCount != newTracMaxCount)
    {
        Seismogram.traceMaxCount = newTracMaxCount;
        var seismogram = ApplicationData.seismogram;
        if (seismogram != null)
        {
            seismogram.updateDrawView();
            seismogram.draw();
        }
    }
}

function webglCanvasOnMouseMove_DrawingSpectrumArea (e)
{
    if (e.buttons == 1)
    {
        var rect = ApplicationData.scene.webglCanvas.getBoundingClientRect();
        var canvasXofCursor = (e.clientX - rect.left) * ApplicationData.ratio;
        var canvasYofCursor = ApplicationData.scene.webglCanvas.height + (rect.top - e.clientY) * ApplicationData.ratio;
        var x = Math.round(canvasXofCursor / ApplicationData.scene.scale - ApplicationData.scene.pivot.x) / Seismogram.userScaleX;
        var t = (canvasYofCursor / ApplicationData.scene.scale - ApplicationData.scene.pivot.y) /
                Seismogram.userScaleT / Seismogram.baseScaleT;
        
        if (!ApplicationData.scene.spectrumArea)
        {
            ApplicationData.scene.spectrumArea = [];
        }
        
        var point = {};
        point.x = x;
        point.t = t;
        
        ApplicationData.scene.spectrumArea.push(point);
        ApplicationData.scene.setSpectrumAreaModel();
        ApplicationData.scene.render();
    }
}

function webglCanvasOnMouseUp_DrawingSpectrumArea (e)
{
    function rotate (A, B, C)
    {
        return (B.x - A.x) * (C.t - B.t) - (B.t - A.t) * (C.x - B.x);
    }
    
    
    function getDistance (point1, point2)
    {
        return Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.t - point2.t, 2));
    }
    
    function isPointInsideArea (point, area)
    {
        if (rotate(area[0], area[1], point) < 0 || rotate(area[0], area[area.length - 1], point) > 0)
        {
            return false;
        }
        
        var left = 1;
        var right = area.length - 1;
        while (right - left > 1)
        {
            var mid = Math.floor((left + right) / 2);
            
            if (rotate(area[0], area[mid], point) < 0)
            {
                right = mid;
            }
            else
            {
                left = mid;
            }
            
        }
        
        return rotate(area[0], point, area[left]) * rotate(area[0], point, area[right]) > 0 ||
                rotate(area[left], area[right], area[0]) * rotate(area[left], area[right], point) > 0;
    }
    
    var spectrumArea = ApplicationData.scene.spectrumArea;
    
    var delta = ApplicationData.ratio / ApplicationData.scene.scale;
    var i = 0;
    while (i < spectrumArea.length - 1)
    {
        var j = i + 1;
        while (j < spectrumArea.length)
        {
            if (getDistance(spectrumArea[i], spectrumArea[j]) < delta)
            {
                spectrumArea.splice(j, 1);
            }
            else
            {
                j++;
            }
        }
        i++;
    }
    
    
    for (i = 1; i < spectrumArea.length; i++)
    {
        if (spectrumArea[i].x < spectrumArea[0].x)
        {
            var tmp = spectrumArea[i];
            spectrumArea[i] = spectrumArea[0];
            spectrumArea[0] = tmp;
        }
    }
    
    var startPoint = {};
    startPoint.x = spectrumArea[0].x;
    startPoint.t = spectrumArea[0].t;
    
    spectrumArea.splice(0, 1);
    spectrumArea.sort(function (p1, p2) {
        var r = rotate(startPoint, p1, p2);
        if (r < 0)
        {
            return 1;
        }
        return -1;
        
    });
    
    spectrumArea.splice(0, 0, startPoint);
    
    var pointStack = [];
    pointStack.push(spectrumArea[0]);
    pointStack.push(spectrumArea[1]);
    
    for (i = 2; i < spectrumArea.length; i++)
    {
        while (rotate(pointStack[pointStack.length - 2], pointStack[pointStack.length - 1], spectrumArea[i]) <= 0)
        {
            pointStack.splice(pointStack.length - 1, 1);
        }
        pointStack.push(spectrumArea[i]);
    }
    
    spectrumArea = ApplicationData.scene.spectrumArea = pointStack;
    
    var currentIndentX = 0;
    var marginX = Seismogram.baseTraceIndent * Seismogram.userTraceIndentScale;
    var insideAreaTraces = ApplicationData.seismogram.traces.map(trace => trace.clone());
    
    i = 0;
    while (i < insideAreaTraces.length)
    {
        var isTraceInsideArea = false;
        
        for (var point of insideAreaTraces[i].points)
        {
            var shiftedPoint = cloneObject(point);
            shiftedPoint.x += currentIndentX;
            if (isPointInsideArea(shiftedPoint, spectrumArea))
            {
                isTraceInsideArea = true;
            }
            else
            {
                point.x = 0;
            }
        }
        
        if (isTraceInsideArea)
        {
            i++;
        }
        else
        {
            insideAreaTraces.splice(i, 1);
        }
        
        currentIndentX += marginX;
    }
    
    ApplicationData.scene.spectrumArea = [];
    ApplicationData.scene.setSpectrumAreaModel();
    ApplicationData.scene.render();
    
    var totalCoefficients = [];
    
    for (var trace of insideAreaTraces)
    {
        var coefficients = FFT.getFourierCoefficients(trace);
        if (totalCoefficients.length == 0)
        {
            totalCoefficients.length = coefficients.length;
            totalCoefficients.fill(0);
        }
        
        coefficients.forEach((value, i) => totalCoefficients[i] += value.getDistance());
    }
    
    totalCoefficients.forEach((value, i, array) => array[i] /= insideAreaTraces.length);
    totalCoefficients.splice(300);

    var div = $("<div>").appendTo($("body"))
            .width(1500)
            .height(620)
            .css({
                "background-color": "white",
                "border": "2px solid black",
                "position": "absolute",
                "left": "50px",
                "top": "50px",
                "z-index": "200"
            });
    
    var canvas = $("<canvas>")
            .appendTo(div)
            .width(1500)
            .height(600)[0];
    
    $("<input>").attr({"type": "button", "value": "REMOVE"}).appendTo(div).click(function () {
        div.remove();
    });
    
    var labels = [];
    for (i = 0; i < 300; i++)
    {
        labels.push(i);
    }
    
    Chart.defaults.global.defaultFontFamily = "Lato";
    Chart.defaults.global.defaultFontSize = 18;
    Chart.defaults.global.defaultFontColor = "black";
    
    
    var chart = new Chart(canvas, {
        type: "bar",
        data: {
            labels: labels,
            datasets: [{
                label: "Fourier Coefficients",
                backgroundColor: "blue",
                data: totalCoefficients,
                //lineTension: 0,
                //fill: false,
                //borderColor: "blue",
                //pointRadius: 0
            }]
        }
    });
}

function webglCanvasOnMouseOut_DrawingSpectrumArea (e)
{

}