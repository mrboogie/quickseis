class Scene
{
    constructor (webglCanvas, axesCanvas)
    {
        this.webglCanvas = webglCanvas.get(0);
        this.axesCanvas = axesCanvas.get(0);
        var webglNames = ["experimental-webgl", "webgl", "webkit-3d", "moz-webgl"];
        this.gl = null;
        for (var i = 0; i < webglNames.length; i++)
        {
            try
            {
                this.gl = this.webglCanvas.getContext(webglNames[i]);
            }
            catch (e)
            {
            }
            if (this.gl)
            {
                break;
            }
        }

        if (!(this.gl))
        {
            alert("Your browser is not WebGL capable");
            return;
        }

        this.reshape();

        this.gl.clearColor(1.0, 1.0, 1.0, 1.0);
        this.gl.lineWidth(1.0);

        var vsh = this.createShader(this.gl.VERTEX_SHADER,
            `attribute vec2 a_position;
         uniform mat4 u_mvp;
         void main(void) {
             gl_Position = u_mvp * vec4(a_position, 0.0, 1.0);
         }`
        );

        var fsh = this.createShader(this.gl.FRAGMENT_SHADER,
            `precision mediump float;
         uniform vec4 u_color;
         void main(void)
         {
             gl_FragColor = u_color;
         }`
        );

        this.effect = this.gl.createProgram();
        this.gl.attachShader(this.effect, vsh);
        this.gl.attachShader(this.effect, fsh);
        this.gl.linkProgram(this.effect);

        if (!this.gl.getProgramParameter(this.effect, this.gl.LINK_STATUS))
        {
            alert("Could not initialize shaders");
        }

        this.gl.useProgram(this.effect);

        this.effect.a_position = this.gl.getAttribLocation(this.effect, "a_position");
        this.effect.u_mvp = this.gl.getUniformLocation(this.effect, "u_mvp");
        this.effect.u_color = this.gl.getUniformLocation(this.effect, "u_color");

        this.gl.enableVertexAttribArray(this.effect.a_position);
        this.pivot = {};
        this.pivot.x = 0; // Bottom left point coordinates of scene.
        this.pivot.y = 0; // Bottom left point coordinates of scene.
        this.scale = 0.03;
        this.models = [];
        this.spectrumArea = null;
        this.setCursorMode(SceneCursorMode.NONE);
    }

    reshape ()
    {
        this.gl.viewport(0, 0, this.webglCanvas.width, this.webglCanvas.height);
        var z = Math.max(this.webglCanvas.width, this.webglCanvas.height);
        this.projection = mat4.ortho(0.0, this.webglCanvas.width, 0.0, this.webglCanvas.height, -z, z);
    }

    createShader (type, src)
    {
        var shader = this.gl.createShader(type);

        this.gl.shaderSource(shader, src);
        this.gl.compileShader(shader);

        if (!(this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)))
        {
            alert(this.gl.getShaderInfoLog(shader));
            return null;
        }

        return shader;
    }

    addTrace (trace)
    {
        var model = {};
        model.visible = true;
        model.vbo = this.gl.createBuffer();
        model.type = SceneModelType.TRACE;
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, model.vbo);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(trace.points), this.gl.STATIC_DRAW);
        model.vertexLength = 2;
        model.vertexCount = trace.points.length / model.vertexLength;

        if (trace.trianglesVertices)
        {
            model.ibo = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, model.ibo);
            this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(trace.trianglesVertices), this.gl.STATIC_DRAW);
            model.ibo.indexNumber = trace.trianglesVertices.length;
        }

        if (!(this.bbox))
        {
            this.bbox =
                {
                    x: trace.bbox.x,
                    y: trace.bbox.y,
                    width: trace.bbox.width,
                    height: trace.bbox.height
                };
        }
        else
        {
            this.bbox =
                {
                    x: Math.min(this.bbox.x, trace.bbox.x),
                    y: Math.min(this.bbox.y, trace.bbox.y),
                    width: Math.max(this.bbox.x + this.bbox.width, trace.bbox.x + trace.bbox.width),
                    height: Math.max(this.bbox.y + this.bbox.height, trace.bbox.y + trace.bbox.height)
                };
            this.bbox.width -= this.bbox.x;
            this.bbox.height -= this.bbox.y;
        }

        model.color = new Float32Array(trace.color);

        model.primitive = this.gl.TRIANGLES;

        this.models.push(model);
    }
    
    setSpectrumAreaModel()
    {
        var i = 0;
        while (i < this.models.length)
        {
            if (this.models[i].type == SceneModelType.SPECTRUM_AREA)
            {
                this.models.splice(i, 1);
            }
            else
            {
                i++;
            }
        }
        
        if (this.spectrumArea.length == 0)
        {
            return;
        }
        
        var spectrumAreaWebGlView = [];
        for (var point of this.spectrumArea)
        {
            spectrumAreaWebGlView.push(point.x * Seismogram.userScaleX);
            spectrumAreaWebGlView.push(point.t * Seismogram.baseScaleT * Seismogram.userScaleT);
        }
        
        var model = {};
        model.visible = true;
        model.vbo = this.gl.createBuffer();
        model.type = SceneModelType.SPECTRUM_AREA;
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, model.vbo);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(spectrumAreaWebGlView), this.gl.STATIC_DRAW);
        model.vertexCount = this.spectrumArea.length;
    
        model.color = new Float32Array([1.0, 0.0, 0.0, 1.0]);
    
        model.primitive = this.gl.TRIANGLES;
    
        this.models.push(model);
    }

    fitToScreen ()
    {
        if (this.bbox)
        {
            this.pivot.x = -this.bbox.x;
            this.pivot.y = -this.bbox.y + (this.webglCanvas.height / this.scale - this.bbox.height);
        }
        this.updateAxes();
    }

    changeZoom (zoom, lockX, lockY)
    {
        if (this.bbox)
        {
            var prevPos = vec3.create();
            var curPos = vec3.create();
            lockY = this.webglCanvas.height - lockY;
            prevPos[0] = this.pivot.x + lockX / this.scale;
            prevPos[1] = this.pivot.y + lockY / this.scale;
            this.scale += zoom * this.scale;
            if (this.scale < 0.001)
                this.scale = 0.001;
            else if (this.scale > 50.0)
                this.scale = 50.0;
            curPos[0] = this.pivot.x + lockX / this.scale;
            curPos[1] = this.pivot.y + lockY / this.scale;
            this.pivot.x += curPos[0] - prevPos[0];
            this.pivot.y += curPos[1] - prevPos[1];

            this.fitToScreen();
            this.render();
        }
    }

    drag (movementX, movementY)
    {
        this.pivot.x += movementX / this.scale;
        this.pivot.y -= movementY / this.scale;
    
        // var margin = 0;
        // if (this.pivot[0] < -this.bbox.x - this.bbox.width + this.webglCanvas.width / this.scale - margin)
        //     this.pivot[0] = -this.bbox.x - this.bbox.width + this.webglCanvas.width / this.scale - margin;
        //
        // if (this.pivot[0] > -this.bbox.x + margin)
        //     this.pivot[0] = -this.bbox.x + margin;
        //
        // if (this.pivot[1] < -this.bbox.y - this.bbox.height + this.webglCanvas.height / this.scale - margin)
        //     this.pivot[1] = -this.bbox.y - this.bbox.height + this.webglCanvas.height / this.scale - margin;
        //
        // if (this.pivot[1] > -this.bbox.y + margin)
        //     this.pivot[1] = -this.bbox.y + margin;

        this.render();
        this.updateAxes();
    }

    calcTransform ()
    {
        var mvp = mat4.create();
        mat4.identity(mvp);
        mat4.scale(mvp, [this.scale, this.scale, this.scale], mvp);
        mat4.translate(mvp, [this.pivot.x, this.pivot.y, 0.0], mvp);
        mat4.multiply(this.projection, mvp, mvp);
        return mvp;
    }

    drawModels ()
    {
        for (var i = 0; i < this.models.length; i++)
        {
            var model = this.models[i];
            this.gl.uniform4fv(this.effect.u_color, model.color);
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, model.vbo);
            this.gl.vertexAttribPointer(this.effect.a_position, 2, this.gl.FLOAT, false, model.vertexLength * 4, 0);
            if (model.ibo)
            {
                this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, model.ibo);
                this.gl.drawElements(model.primitive, model.ibo.indexNumber, this.gl.UNSIGNED_SHORT, 0);
            }
            this.gl.drawArrays(this.gl.LINE_STRIP, 0, model.vertexCount);
        }
    }

    render ()
    {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
        this.gl.uniformMatrix4fv(this.effect.u_mvp, false, this.calcTransform());
        this.drawModels();
    }

    updateAxes ()
    {
        function getAxeDivisionValue (value)
        {
            var degree = 0;
            if (value < 1)
            {
                while (value < 1)
                {
                    value *= 10;
                    degree++;
                }
                return Math.floor(value) / Math.pow(10, degree);
            }
            else
            {
                while (value > 1)
                {
                    value /= 10;
                    degree++;
                }
                return Math.floor(value * 10) * Math.pow(10, degree - 1);
            }
        }
        
        var canvasMarginLeft = this.webglCanvas.getBoundingClientRect().left - this.axesCanvas.getBoundingClientRect().left;
        var canvasMarginTop = this.webglCanvas.getBoundingClientRect().top - this.axesCanvas.getBoundingClientRect().top;
        var ctx = this.axesCanvas.getContext("2d");
        ctx.clearRect(0, 0, this.axesCanvas.width, this.axesCanvas.height);
        ctx.font = 10 * ApplicationData.ratio + "px Arial";
        var count = 10;

        var marginX = Seismogram.baseTraceIndent * Seismogram.userTraceIndentScale;
        var scaleT = Seismogram.baseScaleT * Seismogram.userScaleT;

        var valueOfDivision = getAxeDivisionValue(this.webglCanvas.width / (this.scale * Seismogram.userScaleX * count));
        var number;
        var left;
        if (-this.pivot.x / valueOfDivision < 0)
            left = Math.floor(-this.pivot.x / valueOfDivision / Seismogram.userScaleX) * valueOfDivision;
        else
            left = Math.ceil(-this.pivot.x / valueOfDivision / Seismogram.userScaleX) * valueOfDivision;

        var right = (this.webglCanvas.width / this.scale - this.pivot.x) / Seismogram.userScaleX;
        for (number = left; number < right; number += valueOfDivision)
        {
            ctx.fillText("" + number, (number * Seismogram.userScaleX + this.pivot.x) * this.scale +
                canvasMarginLeft + 0.5 * ctx.measureText("" + number).width, 12 * ApplicationData.ratio);
        }
        
        valueOfDivision = getAxeDivisionValue(this.webglCanvas.height / (-this.scale * scaleT * count));
        var top = Math.round((this.webglCanvas.height / this.scale - this.pivot.y) / scaleT * marginX) / marginX;
        var bot = -this.pivot.y / scaleT;
        for (number = top; number < bot; number += valueOfDivision)
        {
            ctx.fillText(number.toFixed(3), 10, this.webglCanvas.height - (number * scaleT + this.pivot.y) *
                this.scale + ApplicationData.ratio * canvasMarginTop + 10);
        }
    }
    
    clear ()
    {
        this.models.length = 0;
        this.bbox = null;
    }
    
    setCursorMode (cursorMode)
    {
        if (cursorMode == SceneCursorMode.NONE)
        {
            //this.cursorMode = SceneCursorMode.NONE;
            this.webglCanvas.removeEventListener("mousewheel", webglCanvasOnMouseWheel_ChangingScale, false);
            this.webglCanvas.onmousemove = null;
            this.webglCanvas.onmousedown = null;
            this.webglCanvas.onmouseup = null;
            this.webglCanvas.onmouseout = null;
            window.onresize = null;
        }
        else if (cursorMode == SceneCursorMode.DRAGGING_SCENE)
        {
            //this.cursorMode = SceneCursorMode.DRAGGING_SCENE;
            this.webglCanvas.addEventListener("mousewheel", webglCanvasOnMouseWheel_ChangingScale, false);
            this.webglCanvas.onmousemove = webglCanvasOnMouseMove_DraggingScene;
            this.webglCanvas.onmousedown = null;
            this.webglCanvas.onmouseup = null;
            this.webglCanvas.onmouseout = null;
            window.onresize = onWindowResize;
        }
        else if (cursorMode == SceneCursorMode.DRAWING_SPECTRUM_AREA)
        {
            //this.cursorMode = SceneCursorMode.DRAWING_SPECTRUM_AREA;
            this.webglCanvas.addEventListener("mousewheel", webglCanvasOnMouseWheel_ChangingScale, false);
            this.webglCanvas.onmousemove = webglCanvasOnMouseMove_DrawingSpectrumArea;
            this.webglCanvas.onmousedown = null;
            this.webglCanvas.onmouseup = webglCanvasOnMouseUp_DrawingSpectrumArea;
            this.webglCanvas.onmouseout = webglCanvasOnMouseOut_DrawingSpectrumArea;
            window.onresize = onWindowResize;
        }
    }
}