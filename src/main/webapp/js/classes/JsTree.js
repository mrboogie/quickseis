class JsTree
{
    constructor (containerId)
    {
        this.container = $("#" + containerId);
        this.init();
    }

    init ()
    {
        var container = this.container;
        $.ajax({
            url: "/get_all_file_objects",
            type: "post",
            datatype: "json",
            success: function (result)
            {
                var jsonResult = JSON.parse(result);
                container.jstree(jsonResult);
            },
            error: function (xhr)
            {
                console.log(xhr.responseText);
            }
        });
    }

    addNode (id, parent, text, type)
    {
        var node = {
            "id": id,
            "text": text,
            "type": type
        };

        this.container.jstree().create_node(parent, node);
    }

    getSelectedNodes ()
    {
        return this.container.jstree("get_selected", true);
    }

    deselectAllNodes ()
    {
        this.container.jstree("deselect_all");
    }

    getPath (node)
    {
        return this.container.jstree("get_path", node, "/", false);
    }

    getNodeType (node)
    {
        return this.container.jstree("get_type", node);
    }

    getNodeId (node)
    {
        return parseInt(node["li_attr"]["id"].substring(this.getNodeType(node).length));
    }
}

JsTree.NODE_TYPE_DIRECTORY = "directory";
JsTree.NODE_TYPE_PROFILE = "profile";
JsTree.NODE_TYPE_BASE_SEISMOGRAM = "baseSeismogram";
JsTree.NODE_TYPE_FILTERED_SEISMOGRAM = "filteredSeismogram";
JsTree.NODE_TYPE_SUM_SEISMOGRAM = "sumSeismogram";