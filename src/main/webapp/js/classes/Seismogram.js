class Seismogram
{
    constructor (id, traceObjects)
    {
        this.id = id;
        this.traces = [];
        this.tMin = 0;
        this.tMax = 1;
        this.traces = traceObjects.map(traceObject => new Trace(traceObject));
        
        this.webglTraces = this.traces.map(trace => trace.getWebglView());
        
        this.webglTracesDrawView = [];
        this.updateDrawView();
    }
    
    cutTime ()
    {
        this.webglTraces = this.traces.map(trace => trace.getWebglView(this.tMin, this.tMax));
        this.updateDrawView();
    }

    updateDrawView ()
    {
        var traceCount;
        if (Seismogram.traceMaxCount != 0 && Seismogram.traceMaxCount < this.webglTraces.length)
        {
            traceCount = Seismogram.traceMaxCount;
        }
        else
        {
            traceCount = this.webglTraces.length;
        }
    
        this.webglTracesDrawView = [];
        for (var i = 0; i < traceCount; i++)
        {
            this.webglTracesDrawView.push(this.webglTraces[i].clone());
        }
        
        var currentIndentX = 0;
        var scaleX = Seismogram.userScaleX;
        var scaleT = Seismogram.baseScaleT * Seismogram.userScaleT;
        var marginX = Seismogram.baseTraceIndent * Seismogram.userTraceIndentScale;

        for (var j = 0; j < this.webglTracesDrawView.length; j++)
        {
            for (i = 0; i < this.webglTracesDrawView[j].points.length; i += 2)
            {
                this.webglTracesDrawView[j].points[i] = this.webglTracesDrawView[j].points[i] * scaleX + currentIndentX;
                this.webglTracesDrawView[j].points[i + 1] *= scaleT;
            }

            this.webglTracesDrawView[j].updateBbox();
            currentIndentX += marginX;
        }
    }

    sort (traceFieldName)
    {
        this.webglTraces.sort(function (trace1, trace2)
        {
            if (trace1[traceFieldName] < trace2[traceFieldName])
            {
                return -1;
            }
            if (trace1[traceFieldName] > trace2[traceFieldName])
            {
                return 1;
            }
            if (trace1["offsetPointDistance"] < trace2["offsetPointDistance"])
            {
                return -1;
            }
            if (trace1["offsetPointDistance"] > trace2["offsetPointDistance"])
            {
                return 1;
            }
            return 0;
        });
        
        Seismogram.currentSortingTraceField = traceFieldName;
        Seismogram.currentPartNumber = null;
        this.updateDrawView();
    }

    draw ()
    {
        ApplicationData.scene.clear();
        this.webglTracesDrawView.forEach(function (traceDrawView)
        {
            ApplicationData.scene.addTrace(traceDrawView);
        });

        updateWebglCanvasSize();
        ApplicationData.scene.setCursorMode(SceneCursorMode.DRAGGING_SCENE);
        ApplicationData.scene.fitToScreen();
        ApplicationData.scene.render();
    }
}

Seismogram.baseScaleT = -100000;
Seismogram.baseTraceIndent = 1000;
Seismogram.userScaleX = 1;
Seismogram.userScaleT = 1;
Seismogram.userTraceIndentScale = 1;
Seismogram.traceMaxCount = 0;
Seismogram.currentPartNumber = null;
Seismogram.currentSortingTraceField = "sequenceSeismogramNumber";