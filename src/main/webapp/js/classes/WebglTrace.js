class WebglTrace
{
    constructor (trace)
    {
        this.sequenceSeismogramNumber = trace["sequenceSeismogramNumber"];
        this.fieldSeismogramNumber = trace["fieldSeismogramNumber"];
        this.shotPointNumber = trace["shotPointNumber"];
        this.shotPoint = trace["shotPoint"];
        this.depthPoint = trace["depthPoint"];
        this.offsetPoint = trace["offsetPoint"];
        this.offsetPointDistance = trace["offsetPointDistance"];
        this.color = [0.0, 0.0, 0.0, 1.0];
        this.bbox = null;
        this.points = [];
        this.trianglesVertices = [];
        
    
        var triangleList = [];
        var pointNumber = 0;
        var x, t = 0, xOld = 0, tOld = 0;
    
        for (var i = 0; i < trace["points"].length; i++)
        {
            x = trace["points"][i]["x"];
            t = trace["points"][i]["t"];
        
            if (xOld == 0 && x > 0)
            {
                if (pointNumber == 0)
                {
                    this.points.push(0);
                    this.points.push(t);
                    pointNumber++;
                }
                triangleList.push(pointNumber - 1);
                triangleList.push(pointNumber);
            }
            else if (xOld < 0 && x > 0)
            {
                this.points.push(0);
                this.points.push((t * xOld - tOld * x) / (xOld - x));
                pointNumber++;
                triangleList.push(pointNumber - 1);
                triangleList.push(pointNumber);
            }
            else if (xOld > 0 && x > 0)
            {
                triangleList.push(pointNumber);
            }
            else if (xOld > 0 && x == 0)
            {
                triangleList.push(pointNumber);
                this.addTrianglesVertices(triangleList);
                triangleList.length = 0;
            }
            else if (xOld > 0 && x < 0)
            {
                this.points.push(0);
                this.points.push((t * xOld - tOld * x) / (xOld - x));
                triangleList.push(pointNumber);
                pointNumber++;
                this.addTrianglesVertices(triangleList);
                triangleList.length = 0;
            }
    
            this.points.push(x);
            this.points.push(t);
            pointNumber++;
            xOld = x;
            tOld = t;
        }
    
        if (triangleList.length != 0)
        {
            this.points.push(0);
            this.points.push(t);
            triangleList.push(pointNumber);
            this.addTrianglesVertices(triangleList);
            triangleList.length = 0;
        }
    }
    
    addTrianglesVertices (triangleList)
    {
        for (var i = 1; i < triangleList.length - 1; i++)
        {
            this.trianglesVertices.push(triangleList[0]);
            this.trianglesVertices.push(triangleList[i]);
            this.trianglesVertices.push(triangleList[i + 1]);
        }
    }
    
    clone ()
    {
        var webglTrace = Object.assign(Object.create(Object.getPrototypeOf(this)), this);
        webglTrace.points = this.points.slice();
        webglTrace.trianglesVertices = this.trianglesVertices.slice();
        return webglTrace;
    }
    
    updateBbox ()
    {
        var xMin = this.points[0], tMin = this.points[1], xMax = this.points[0], tMax = this.points[1];
        
        for (var i = 2; i < this.points.length; i += 2)
        {
            if (this.points[i] > xMax)
            {
                xMax = this.points[i];
            }
            if (this.points[i] < xMin)
            {
                xMin = this.points[i];
            }
            if (this.points[i + 1] > tMax)
            {
                tMax = this.points[i + 1];
            }
            if (this.points[i + 1] < tMin)
            {
                tMin = this.points[i + 1];
            }
        }
        
        this.bbox = {
            x: xMin,
            y: tMin,
            width: xMax - xMin,
            height: tMax - tMin
        }
    }
}