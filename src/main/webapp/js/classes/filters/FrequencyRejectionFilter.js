class FrequencyRejectionFilter extends BaseFilter
{
    constructor ()
    {
        super();

        this.frequencyInputText = this.addSetting("Frequency", $("<input>").attr("type", "text"));
        this.bandwidthInputText = this.addSetting("Bandwidth", $("<input>").attr("type", "text"));
        this.startXInputText = this.addSetting("Start X", $("<input>").attr("type", "text"));
        this.finishXInputText = this.addSetting("Finish X", $("<input>").attr("type", "text"));
    }

    toJsonObject ()
    {
        var jsonObject = super.toJsonObject();
        jsonObject["filter_class_name"] = this.constructor.name;
        jsonObject["arguments"] = [this.frequencyInputText.val(), this.bandwidthInputText.val(), this.startXInputText.val(), this.finishXInputText.val()];

        return jsonObject;
    }
}
