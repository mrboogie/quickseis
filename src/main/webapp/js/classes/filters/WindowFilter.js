class WindowFilter extends BaseFilter
{
    constructor ()
    {
        super();
        this.windowSizeInputText = this.addSetting("Window size", $("<input>").attr("type", "text"));
        this.maxXInputText = this.addSetting("max X", $("<input>").attr("type", "text"));
    }
    
    toJsonObject ()
    {
        var jsonObject = super.toJsonObject();
        jsonObject["filter_class_name"] = this.constructor.name;
        jsonObject["arguments"] = [this.windowSizeInputText.val(), this.maxXInputText.val()];
        
        return jsonObject;
    }
}