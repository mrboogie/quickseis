class BaseFilter
{
    constructor ()
    {
        var container = $("<div>").addClass("filter-wrapper-div");
        var headerDiv = $("<div>").addClass("filter-header").appendTo(container);
        var settingsDiv = $("<div>").addClass("filter-settings").appendTo(container);
        var settingsTable = $("<table>").addClass("filter-settings-table").appendTo(settingsDiv);

        this.container = container;
        this.settingsTable = settingsTable;

        var filterName;

        var constructorName = this.constructor.name;

        ApplicationData.filters.filterMap.forEach(function (filterMapElement)
        {
            if (filterMapElement.filterClassName == constructorName)
            {
                filterName = filterMapElement.filterName;
            }
        });

        if (filterName == undefined)
        {
            console.error("Couldn't find filter by class name");
            filterName = constructorName;
        }

        var filterNameDiv = $("<div>").addClass("filter-name-div").text(filterName).appendTo(headerDiv);
        var filterHandleDiv = $("<div>").addClass("filter-handle-div").appendTo(headerDiv);
        var settingsButton = $("<button>").addClass("filter-settings-button").appendTo(filterHandleDiv);
        var settingsButtonImage = $("<img>").addClass("filter-settings-button-icon").attr("src", "img/settings.png").appendTo(settingsButton);
        var removeButton = $("<button>").addClass("filter-remove-button").appendTo(filterHandleDiv);
        var removeButtonImage = $("<img>").addClass("filter-remove-button-icon").attr("src", "img/minus.png").appendTo(removeButton);

        removeButton.click(function ()
        {
            container.remove();
        });

        var sortingTraceFieldSelect = $("<select>");

        $(ApplicationData.filters.sortingTraceFields).each(function ()
        {
            $("<option>").attr("value", this.value).text(this.text).appendTo(sortingTraceFieldSelect);
        });

        this.sortingTraceFieldSelect = this.addSetting("Sorting field", sortingTraceFieldSelect);
    }

    addSetting (label, control)
    {
        var row = $("<tr>").appendTo(this.settingsTable);
        var labelCell = $("<td>").text(label).appendTo(row);
        var controlCell = $("<td>").appendTo(row);
        control.appendTo(controlCell);
        return control;
    }

    toJsonObject ()
    {
        var jsonObject = {};
        jsonObject["sorting_traces_field_name"] = this.sortingTraceFieldSelect.val();

        return jsonObject;
    }
}