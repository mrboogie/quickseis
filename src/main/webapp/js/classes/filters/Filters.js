class Filters extends Array
{
    constructor (containerId)
    {
        super();
        this.container = $("#" + containerId);

        this.filterMap = [
            {filterName: "Cutting", filterClassName: "CutTimeFilter"},
            {filterName: "Scaling", filterClassName: "ScaleFilter"},
            {filterName: "Frequency Rejection", filterClassName: "FrequencyRejectionFilter"},
            {filterName: "Bandpass", filterClassName: "BandpassFilter"},
            {filterName: "Window", filterClassName: "WindowFilter"}
        ];

        this.sortingTraceFields = [
            {value: "shot_point", text: "SP"},
            {value: "depth_point", text: "DP"},
            {value: "offset_point", text: "OP"}
        ];
    }

    push (filter)
    {
        super.push(filter);
        this.container.append(filter.container);
    }

    apply (seismogramId, partNumber)
    {
        var filters = this;
        var filterObjects = [];
        for (var i = 0; i < filters.length; i++)
        {
            filterObjects.push(filters[i].toJsonObject());
        }

        var query = "/filtrate?seismogram_id=" + seismogramId +
                (partNumber == null ? "" : "&part_number=" + partNumber) +
                (partNumber != null && Seismogram.traceMaxCount != 0 ? "&trace_max_count=" + Seismogram.traceMaxCount : "");
        

        $.post({
            url: query,
            contentType: "application/json",
            data: JSON.stringify(filterObjects)
        })
            .then(function (traceObjects)
            {
                if (partNumber != null)
                {
                    traceObjects = JSON.parse(traceObjects);
                    ApplicationData.seismogram = new Seismogram(seismogramId, traceObjects);
                    ApplicationData.seismogram.draw();
                }
                else
                {
                    alert("Done! Please refresh page")
                }
            })
            .catch(function (reason)
            {
                console.log(reason);
            });

        return true;
    }
}