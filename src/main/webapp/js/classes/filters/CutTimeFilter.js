class CutTimeFilter extends BaseFilter
{
    constructor ()
    {
        super();
        this.maxTInputText = this.addSetting("Max time", $("<input>").attr("type", "text"));
    }

    toJsonObject ()
    {
        var jsonObject = super.toJsonObject();
        jsonObject["filter_class_name"] = this.constructor.name;
        jsonObject["arguments"] = [this.maxTInputText.val()];

        return jsonObject;
    }
}
