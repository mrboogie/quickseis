class BandpassFilter extends BaseFilter
{
    constructor ()
    {
        super();
        
        this.minFrequencyInputText = this.addSetting("min frequency", $("<input>").attr("type", "text"));
        this.maxFrequencyInputText = this.addSetting("max frequency", $("<input>").attr("type", "text"));
    }
    
    toJsonObject ()
    {
        var jsonObject = super.toJsonObject();
        jsonObject["filter_class_name"] = this.constructor.name;
        jsonObject["arguments"] = [this.minFrequencyInputText.val(), this.maxFrequencyInputText.val()];
        
        return jsonObject;
    }
}
