class ScaleFilter extends BaseFilter
{
    constructor ()
    {
        super();

        this.scaleXInputText = this.addSetting("Scale X", $("<input>").attr("type", "text"));
        this.scaleTInputText = this.addSetting("Scale T", $("<input>").attr("type", "text"));
    }

    toJsonObject ()
    {
        var jsonObject = super.toJsonObject();
        jsonObject["filter_class_name"] = this.constructor.name;
        jsonObject["arguments"] = [this.scaleXInputText.val(), this.scaleTInputText.val()];

        return jsonObject;
    }
}
