class FFT
{
    static computeZeroCount (n)
    {
        return Math.round(Math.exp(Math.log(2) * (Math.floor(Math.log(n) / Math.log(2)) + 1))) - n;
    }
    
    static w (k, n, isDirect)
    {
        if (k % n == 0)
        {
            return new ComplexDigit(1);
        }
        
        var arg = 0.0;
        
        if (isDirect)
        {
            arg = 2 * Math.PI * k / n;
        }
        else
        {
            arg = -2 * Math.PI * k / n;
        }
        return new ComplexDigit(Math.cos(arg), Math.sin(arg));
    }
    
    static fft (n, points, isDirect)
    {
        var result = [], i;
        if (n == 2)
        {
            result[0] = new ComplexDigit(points[0].re + points[1].re, points[0].im + points[1].im);
            result[1] = new ComplexDigit(points[0].re - points[1].re, points[0].im - points[1].im);
        }
        
        else
        {
            var x_even = [];
            var x_odd = [];
            
            for (i = 0; i < n / 2; i++)
            {
                x_even[i] = points[2 * i];
                x_odd[i] = points[2 * i + 1];
            }
            var X_even = this.fft(n / 2, x_even, isDirect);
            var X_odd = this.fft(n / 2, x_odd, isDirect);
            for (i = 0; i < n / 2; i++)
            {
                result[i] = new ComplexDigit(X_even[i].re + this.w(i, n, isDirect).re * X_odd[i].re - this.w(i, n, isDirect).im * X_odd[i].im,
                        X_even[i].im + this.w(i, n, isDirect).re * X_odd[i].im + this.w(i, n, isDirect).im * X_odd[i].re);
                
                result[i + n / 2] = new ComplexDigit(X_even[i].re - this.w(i, n, isDirect).re * X_odd[i].re + this.w(i, n, isDirect).im * X_odd[i].im,
                        X_even[i].im - this.w(i, n, isDirect).re * X_odd[i].im - this.w(i, n, isDirect).im * X_odd[i].re);
            }
        }
        
        return result;
    }
    
    static getFourierCoefficients (trace)
    {
        var points = trace.points;
        var complexPoints = [];
        
        for (var point of points)
        {
            complexPoints.push(new ComplexDigit(point.x));
        }
        
        var zeroCount = this.computeZeroCount(points.length);
        
        for (var i = 0; i < zeroCount; i++)
        {
            complexPoints[points.length + i] = new ComplexDigit(0);
        }
    
        var coefficients = this.fft(points.length + zeroCount, complexPoints, true);
        
        for (i = 0; i < coefficients.length; i++)
        {
            coefficients[i].re /= Math.sqrt(coefficients.length);
            coefficients[i].im /= Math.sqrt(coefficients.length);
        }
        
        return coefficients;
    }
}