class ComplexDigit
{
    constructor (re, im)
    {
        this.re = re;
        this.im = im == undefined ? 0 : im;
    }
    
    getDistance ()
    {
        return Math.sqrt(Math.pow(this.re, 2) + Math.pow(this.im, 2));
    }
}