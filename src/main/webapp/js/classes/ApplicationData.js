class ApplicationData
{
    static getWindowRatio ()
    {
        var ctx = $("#axes-canvas").get(0).getContext("2d");
        return (window.devicePixelRatio || 1) / (ctx.webkitBackingStorePixelRatio ||
            ctx.mozBackingStorePixelRatio || ctx.msBackingStorePixelRatio ||
            ctx.oBackingStorePixelRatio || ctx.backingStorePixelRatio || 1);
    }
}
