class Trace
{
    constructor (traceObject)
    {
        this.sequenceSeismogramNumber = traceObject["sequenceSeismogramNumber"];
        this.fieldSeismogramNumber = traceObject["fieldSeismogramNumber"];
        this.shotPointNumber = traceObject["shotPointNumber"];
        this.shotPoint = traceObject["shotPoint"];
        this.depthPoint = traceObject["depthPoint"];
        this.offsetPoint = traceObject["offsetPoint"];
        this.offsetPointDistance = traceObject["offsetPointDistance"];
        this.points = traceObject["points"].map(a => Object.assign({}, a));
    }
    
    clone ()
    {
        var trace = cloneObject(this);
        trace.points = this.points.map(a => Object.assign({}, a));
        return trace;
    }
    
    getWebglView (tMin, tMax)
    {
        if (tMin == undefined)
        {
            return new WebglTrace(this);
        }
        var trace = this.clone();
        var points = trace["points"];
        
        var i = 0;
        while (i < points.length)
        {
            if (points[i]["t"] < tMin || points[i]["t"] > tMax)
            {
                points.splice(i, 1);
            }
            else
            {
                i++;
            }
        }
        
        return new WebglTrace(trace);
    }
}