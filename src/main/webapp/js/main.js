$(document).ready(function ()
{
    ApplicationData.jstree = new JsTree("jstree");
    ApplicationData.jstree.container.click(function (e)
    {
        if (this == e.target)
        {
            ApplicationData.jstree.deselectAllNodes();
        }
    });

    ApplicationData.ratio = ApplicationData.getWindowRatio();
    ApplicationData.scene = new Scene($("#webgl-canvas"), $("#axes-canvas"));
    
    $("#scale-x-input-number").bind('keyup mouseup', onScaleXInputNumberChange);
    $("#scale-t-input-number").bind('keyup mouseup', onScaleTInputNumberChange);
    $("#trace-indent-scale-input-number").bind('keyup mouseup', onTraceIndentScaleInputNumberChange);
    $("#t-min-input-number").bind('keyup mouseup', onTMinInputNumberChange);
    $("#t-max-input-number").bind('keyup mouseup', onTMaxInputNumberChange);
    $("#trace-max-count-input-number").bind('keyup mouseup', onTraceMaxCountInputNumberChange);
    
    
    ApplicationData.seismogram = null;
    ApplicationData.filters = new Filters("filters-div");

    $("#add-filter-button").click(addFilterButtonOnClick);

    $(ApplicationData.filters.filterMap).each(function ()
    {
        $("#filter-select").append($("<option>").attr("value", this.filterClassName).text(this.filterName));
    });

    $("#draw-full-seismogram-button").click(drawFullSeismogramButtonOnClick);
    $("#draw-partial-seismogram-button").click(drawPartialSeismogramButtonOnClick);
    $("#increase-part-number-button").click(increasePartNumberButtonOnClick);
    $("#decrease-part-number-button").click(decreasePartNumberButtonOnClick);
    $("#full-apply-filters-button").click(fullApplyFiltersButtonOnClick);
    $("#partial-apply-filters-button").click(partialApplyFiltersButtonOnClick);


    $("#sortBySNTd").click(sortBySNTdOnClick);
    $("#sortByFNTd").click(sortByFNTdOnClick);
    $("#sortBySPNTd").click(sortBySPNTdOnClick);
    $("#sortBySPTd").click(sortBySPTdOnClick);
    $("#sortByDPTd").click(sortByDPTdOnClick);
    $("#sortByOPTd").click(sortByOPTdOnClick);
    $("#sortByOPDTd").click(sortByOPDTdOnClick);
    


    // Alexandra's document.onready part
    $("#personal-information-button").click(function ()
    {
        $("#personal-information").show();
    });
    $("#personal-information").click(function ()
    {
        $("#personal-information").show();
    });
    $(document).mouseup(function ()
    {
        $("#personal-information").hide();
    });
    $("#show-tree-button").click(function ()
    {
        $("#tree-div-wrapper").toggle();
    });
    $("#add-directory-button").click(function ()
    {
        $("#blackout").toggle();
        $("#jstree-handle-add-directory").toggle();
    });
    $("#add-directory-button-ok").click(function ()
    {
        $("#blackout").toggle();
        $("#jstree-handle-add-directory").toggle();
        addDirectoryButtonOkOnClick(this);
    });
    $("#add-directory-button-cancel").click(function ()
    {
        $("#blackout").toggle();
        $("#jstree-handle-add-directory").toggle();
    });
    $("#add-seismogram-button").click(function ()
    {
        $("#blackout").toggle();
        $("#jstree-handle-add-sesmogram").toggle();
    });
    $("#add-seismogram-button-cancel").click(function ()
    {
        $("#blackout").toggle();
        $("#jstree-handle-add-sesmogram").toggle();
    });
    $("#add-seismogram-button-ok").click(function ()
    {
        $("#blackout").toggle();
        $("#jstree-handle-add-sesmogram").toggle();
        addSeismogramButtonOkOnClick(this);
    });


});