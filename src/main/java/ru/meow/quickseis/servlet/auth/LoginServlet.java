package ru.meow.quickseis.servlet.auth;

import ru.meow.quickseis.database.DatabaseConnector;
import ru.meow.quickseis.entity.user.User;
import ru.meow.quickseis.serverdata.ServerData;
import ru.meow.quickseis.serverdata.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ru.meow.quickseis.exception.ServerException.handleException;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet
{
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        try
        {
            DatabaseConnector databaseConnector = new DatabaseConnector();
            databaseConnector.openConnection();
            String userLogin = request.getParameter(User.LOGIN_REQUEST_PARAMETER_NAME);
            String userPassword = request.getParameter(User.PASSWORD_REQUEST_PARAMETER_NAME);
            User user;
            Session session;
            Cookie cookie;
            if (databaseConnector.isUserExist(userLogin, userPassword))
            {
                user = new User(userLogin, userPassword);
                session = new Session();
                ServerData.users.put(session.getId(), user);
                cookie = new Cookie(Session.REQUEST_COOKIE_NAME, session.getId());
                cookie.setMaxAge(Session.SESSION_LIFETIME);
                response.addCookie(cookie);
                response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
                response.sendRedirect(ServerData.MAIN_PAGE_URL);
            }
            else
            {
                response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
                response.sendRedirect(ServerData.LOGIN_PAGE_URL);
            }
            databaseConnector.closeConnection();
        }
        catch (Exception e)
        {
            handleException(e, response);
        }
        
    }
}
