package ru.meow.quickseis.servlet.auth;

import ru.meow.quickseis.serverdata.ServerData;
import ru.meow.quickseis.serverdata.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = "/logout")
public class LogoutServlet extends HttpServlet
{
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        Cookie[] cookies = request.getCookies();
        String sessid;
        
        if (cookies != null)
        {
            for (Cookie cookie : cookies)
            {
                if (cookie.getName().equals(Session.REQUEST_COOKIE_NAME))
                {
                    sessid = cookie.getValue();
                    if (ServerData.users.containsKey(sessid))
                    {
                        ServerData.users.remove(sessid);
                    }
                    cookie.setMaxAge(Session.REMOVE_SESSION_TIME);
                    response.addCookie(cookie);
                }
            }
        }
        
        response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
        response.sendRedirect(ServerData.LOGIN_PAGE_URL);
    }
}
