package ru.meow.quickseis.servlet;

import com.google.gson.Gson;
import ru.meow.quickseis.database.DatabaseConnector;
import ru.meow.quickseis.entity.fileobject.Directory;
import ru.meow.quickseis.entity.fileobject.Profile;
import ru.meow.quickseis.entity.fileobject.Seismogram;
import ru.meow.quickseis.entity.user.User;
import ru.meow.quickseis.jstree.JsTree;
import ru.meow.quickseis.jstree.JsTreeNode;
import ru.meow.quickseis.serverdata.ServerData;
import ru.meow.quickseis.serverdata.Session;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static ru.meow.quickseis.exception.ServerException.handleException;

@WebServlet(urlPatterns = "/get_all_file_objects")
public class GetAllFileObjectsServlet extends HttpServlet
{
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        try
        {
            String sessid = Session.getSessidFromRequest(request);
            User user = ServerData.users.get(sessid);
            DatabaseConnector databaseConnector = new DatabaseConnector();
            databaseConnector.openConnection(user);
            JsTree jsTree = new JsTree();
            ArrayList<JsTreeNode> jsTreeNodes = databaseConnector.getAllUserFileObjectsAsJsTreeNodes();
            
            for (JsTreeNode jsTreeNode : jsTreeNodes)
            {
                jsTree.addNode(jsTreeNode);
            }
            
            databaseConnector.closeConnection();
            String jsonJsTreeString = new Gson().toJson(jsTree);
            response.getWriter().print(jsonJsTreeString);
        }
        catch (Exception e)
        {
            handleException(e, response);
        }
    }
}
