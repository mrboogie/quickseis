package ru.meow.quickseis.servlet;

import ru.meow.quickseis.database.DatabaseConnector;
import ru.meow.quickseis.entity.fileobject.Seismogram;
import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.entity.trace.TraceList;
import ru.meow.quickseis.entity.user.User;
import ru.meow.quickseis.exception.ServerException;
import ru.meow.quickseis.serverdata.ServerData;
import ru.meow.quickseis.serverdata.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import static ru.meow.quickseis.exception.ServerException.handleException;

@WebServlet(urlPatterns = "/get_seismogram_traces")
public class GetSeismogramTracesServlet extends HttpServlet
{
    
    protected TraceList getSeismogramTraces (HttpServletRequest request)
            throws ServerException, SQLException, IOException, ClassNotFoundException, IllegalAccessException,
            IntrospectionException, InvocationTargetException
    {
        int seismogramId = Integer.valueOf(request.getParameter(Seismogram.SEISMOGRAM_ID_PARAMATER_NAME));
        Integer partNumber = null;
    
        if (request.getParameterMap().containsKey(Seismogram.PART_NUMBER_PARAMATER_NAME))
        {
            partNumber = Integer.valueOf(request.getParameter(Seismogram.PART_NUMBER_PARAMATER_NAME));
        }
        
        String sortingFieldName = null;
        if (request.getParameterMap().containsKey(Seismogram.SORTING_FIELD_NAME_PARAMATER_NAME))
        {
            sortingFieldName = request.getParameter(Seismogram.SORTING_FIELD_NAME_PARAMATER_NAME);
        }
    
        Integer traceMaxCount = null;
        if (request.getParameterMap().containsKey(Seismogram.TRACE_MAX_COUNT_PARAMATER_NAME))
        {
            traceMaxCount = Integer.valueOf(request.getParameter(Seismogram.TRACE_MAX_COUNT_PARAMATER_NAME));
        }
        
        User user = ServerData.users.get(Session.getSessidFromRequest(request));
        DatabaseConnector databaseConnector = new DatabaseConnector();
        databaseConnector.openConnection(user);
    
        TraceList traces;
    
        if (partNumber == null)
        {
            traces = databaseConnector.getSeismogramTraces(seismogramId, sortingFieldName);
        }
        else if (traceMaxCount == null)
        {
            traces = databaseConnector.getSeismogramTraces(seismogramId, sortingFieldName, partNumber);
        }
        else
        {
            traces = databaseConnector.getSeismogramTraces(seismogramId, sortingFieldName, partNumber, traceMaxCount);
        }
        
        databaseConnector.closeConnection();
        
        return traces;
    }
    
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        try
        {
            TraceList traces = getSeismogramTraces(request);
            String jsonSeismogramTracesString = traces.getJsonStringView();
            response.getWriter().print(jsonSeismogramTracesString);
        }
        catch (Exception e)
        {
            handleException(e, response);
        }
    }
}
