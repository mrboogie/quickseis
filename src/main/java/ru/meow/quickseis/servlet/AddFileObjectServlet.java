package ru.meow.quickseis.servlet;

import ru.meow.quickseis.database.DatabaseConnector;
import ru.meow.quickseis.entity.fileobject.Directory;
import ru.meow.quickseis.entity.fileobject.Profile;
import ru.meow.quickseis.entity.fileobject.Seismogram;
import ru.meow.quickseis.entity.trace.TraceList;
import ru.meow.quickseis.entity.user.User;
import ru.meow.quickseis.exception.ServerException;
import ru.meow.quickseis.serverdata.ServerData;
import ru.meow.quickseis.serverdata.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

import static ru.meow.quickseis.exception.ServerException.handleException;


@MultipartConfig
@WebServlet(urlPatterns = "/add_file_object")
public class AddFileObjectServlet extends HttpServlet
{
    private static final String FILE_OBJECT_TYPE_PARAMETER_NAME = "file_object_type";
    
    public enum FILE_OBJECT_TYPE
    {
        DIRECTORY, PROFILE, SEISMOGRAM;
        
        public static FILE_OBJECT_TYPE fromString (String fileObjectTypeString)
        {
            for (FILE_OBJECT_TYPE fileObjectType : FILE_OBJECT_TYPE.values())
            {
                if (fileObjectType.toString().equals(fileObjectTypeString))
                {
                    return fileObjectType;
                }
            }
            
            return null;
        }
        
        @Override
        public String toString ()
        {
            switch (this)
            {
                case DIRECTORY:
                    return Directory.FILE_OBJECT_TYPE;
                case PROFILE:
                    return Profile.FILE_OBJECT_TYPE;
                case SEISMOGRAM:
                    return Seismogram.FILE_OBJECT_TYPE;
            }
            
            return super.toString();
        }
    }
    
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        
        String fileObjectTypeString = request.getParameter(FILE_OBJECT_TYPE_PARAMETER_NAME);
        FILE_OBJECT_TYPE fileObjectType = FILE_OBJECT_TYPE.fromString(fileObjectTypeString);
        String sessid = Session.getSessidFromRequest(request);
        User user = ServerData.users.get(sessid);
        DatabaseConnector databaseConnector;
        int lastInsertId = -1;
        
        try
        {
            if (fileObjectType == null)
            {
                throw new ServerException();
            }
            
            databaseConnector = new DatabaseConnector();
            databaseConnector.openConnection(user);
            
            if (fileObjectType == FILE_OBJECT_TYPE.DIRECTORY)
            {
                String directoryName = request.getParameter(Directory.DIRECTORY_NAME_PARAMATER_NAME);
                Integer parentDirectoryId;
                
                if (request.getParameterMap().containsKey(Directory.PARENT_DIRECTORY_ID_PARAMATER_NAME))
                {
                    parentDirectoryId = Integer.valueOf(request.getParameter(Directory.PARENT_DIRECTORY_ID_PARAMATER_NAME));
                }
                else
                {
                    parentDirectoryId = null;
                }
                
                lastInsertId = databaseConnector.addDirectory(directoryName, parentDirectoryId);
            }
            else if (fileObjectType == FILE_OBJECT_TYPE.PROFILE)
            {
                String profileName = request.getParameter(Profile.PROFILE_NAME_PARAMATER_NAME);
                int directoryId = Integer.valueOf(request.getParameter(Profile.DIRECTORY_ID_PARAMATER_NAME));
                lastInsertId = databaseConnector.addProfile(profileName, directoryId);
            }
            else if (fileObjectType == FILE_OBJECT_TYPE.SEISMOGRAM)
            {
                String seismogramName = request.getParameter(Seismogram.SEISMOGRAM_NAME_PARAMATER_NAME);
                int profileId = Integer.valueOf(request.getParameter(Seismogram.PROFILE_ID_PARAMATER_NAME));
                InputStream inputStream = request.getPart(Seismogram.SEISMIC_FILE_PARAMATER_NAME).getInputStream();
                lastInsertId = databaseConnector.addSeismogram(seismogramName, profileId,
                        TraceList.getFromInputStream(inputStream), Seismogram.SeismogramType.BASE);
            }
            
            response.getWriter().print(lastInsertId);
            
            databaseConnector.closeConnection();
        }
        catch (Exception e)
        {
            handleException(e, response);
        }
    }
}
