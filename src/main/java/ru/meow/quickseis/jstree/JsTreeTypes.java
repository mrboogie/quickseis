package ru.meow.quickseis.jstree;

public class JsTreeTypes
{
    private JsTreeType directory = new JsTreeType(JsTree.DIRECTORY_ICON);
    private JsTreeType profile = new JsTreeType(JsTree.PROFILE_ICON);
    private JsTreeType baseSeismogram = new JsTreeType(JsTree.BASE_SEISMOGRAM_ICON);
    private JsTreeType filteredSeismogram = new JsTreeType(JsTree.FILTERED_SEISMOGRAM_ICON);
    private JsTreeType sumSeismogram = new JsTreeType(JsTree.SUM_SEISMOGRAM_ICON);

    public JsTreeTypes () {}
}
