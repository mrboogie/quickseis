package ru.meow.quickseis.jstree;

import ru.meow.quickseis.entity.fileobject.Directory;
import ru.meow.quickseis.entity.fileobject.Profile;
import ru.meow.quickseis.entity.fileobject.Seismogram;

import java.util.Arrays;
import java.util.List;

public class JsTree
{
    public static final String DIRECTORY_ICON = "/img/directory.ico";
    public static final String PROFILE_ICON = "/img/profile.ico";
    public static final String BASE_SEISMOGRAM_ICON = "jstree-file";
    public static final String FILTERED_SEISMOGRAM_ICON = "/img/filtered.ico";
    public static final String SUM_SEISMOGRAM_ICON = "/img/sum.png";
    
    private List<String> plugins = Arrays.asList("themes", "types");
    private JsTreeTypes types = new JsTreeTypes();
    private JsTreeCore core = new JsTreeCore();

    public JsTree () {}

    public void addNode (JsTreeNode node)
    {
        core.addNode(node);
    }
}
