package ru.meow.quickseis.util;

public class ComplexDigit
{
    public double re;
    public double im;
    
    public ComplexDigit (double x, double y)
    {
        re = x;
        im = y;
    }
    
    public ComplexDigit (double x)
    {
        re = x;
        im = 0;
    }
}
