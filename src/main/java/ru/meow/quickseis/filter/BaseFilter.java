package ru.meow.quickseis.filter;

import ru.meow.quickseis.entity.trace.Trace;

public abstract class BaseFilter
{
    protected String sortingTracesFieldName;
    
    public BaseFilter (String sortingTracesFieldParameterName)
    {
        this.sortingTracesFieldName = Trace.getTraceFieldNameFromRequestParameter(sortingTracesFieldParameterName);
    }
}
