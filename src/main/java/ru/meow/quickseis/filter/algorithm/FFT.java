package ru.meow.quickseis.filter.algorithm;

import ru.meow.quickseis.entity.trace.Point;
import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.util.ComplexDigit;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class FFT
{
    public static int computeZeroCount (int n)
    {
        return (int)Math.round(Math.exp(Math.log(2) * (Math.floor(Math.log(n) / Math.log(2)) + 1))) - n;
    }
    
    
    private static ComplexDigit w (int k, int n, boolean isDirect)
    {
        if (k % n == 0)
        {
            return new ComplexDigit(1);
        }
        
        double arg;
        
        if (isDirect)
        {
            arg = 2 * Math.PI * k / n;
        }
        else
        {
            arg = -2 * Math.PI * k / n;
        }
        return new ComplexDigit(Math.cos(arg), Math.sin(arg));
    }
    
    private static ComplexDigit[] fft (int n, ComplexDigit[] points, boolean isDirect)
    {
        ComplexDigit[] result = new ComplexDigit[n];
        if (n == 2)
        {
            result[0] = new ComplexDigit(points[0].re + points[1].re, points[0].im + points[1].im);
            result[1] = new ComplexDigit(points[0].re - points[1].re, points[0].im - points[1].im);
        }
        
        else
        {
            ComplexDigit[] x_even = new ComplexDigit[n / 2];
            ComplexDigit[] x_odd = new ComplexDigit[n / 2];
            
            for (int i = 0; i < n / 2; i++)
            {
                x_even[i] = points[2 * i];
                x_odd[i] = points[2 * i + 1];
            }
            ComplexDigit[] X_even = fft(n / 2, x_even, isDirect);
            ComplexDigit[] X_odd = fft(n / 2, x_odd, isDirect);
            for (int i = 0; i < n / 2; i++)
            {
                result[i] = new ComplexDigit(X_even[i].re + w(i, n, isDirect).re * X_odd[i].re - w(i, n, isDirect).im * X_odd[i].im,
                        X_even[i].im + w(i, n, isDirect).re * X_odd[i].im + w(i, n, isDirect).im * X_odd[i].re);
                
                result[i + n / 2] = new ComplexDigit(X_even[i].re - w(i, n, isDirect).re * X_odd[i].re + w(i, n, isDirect).im * X_odd[i].im,
                        X_even[i].im - w(i, n, isDirect).re * X_odd[i].im - w(i, n, isDirect).im * X_odd[i].re);
                
                
            }
        }
        
        return result;
    }
    
    public static ComplexDigit[] getFourierCoefficientsFromPoints (ArrayList<Point> points)
    {
        int zeroCount = computeZeroCount(points.size());
        
        ComplexDigit[] complexPoints = new ComplexDigit[points.size() + zeroCount];
        
        
        for (int i = 0; i < points.size(); i++)
        {
            complexPoints[i] = new ComplexDigit(points.get(i).getX());
        }
        
        for (int i = 0; i < zeroCount; i++)
        {
            complexPoints[points.size() + i] = new ComplexDigit(0);
        }
    
        ComplexDigit[] coefficients = fft(complexPoints.length, complexPoints, true);
        
        for (int i = 0; i < coefficients.length; i++)
        {
            coefficients[i].im /= Math.sqrt((double)coefficients.length);
            coefficients[i].re /= Math.sqrt((double)coefficients.length);
        }
        
        return coefficients;
    }
    
    public static void updatePointsFromFourierCoefficients (ComplexDigit[] fourierCoefficients, ArrayList<Point> points)
    {
        ComplexDigit[] complexPoints = fft(fourierCoefficients.length, fourierCoefficients, false);
        for (int i = 0; i < points.size(); i++)
        {
            points.get(i).setX((float) complexPoints[i].re / (float) Math.sqrt((double)points.size()));
        }
    }
}
