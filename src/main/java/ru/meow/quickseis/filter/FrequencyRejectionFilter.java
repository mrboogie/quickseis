package ru.meow.quickseis.filter;


import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.entity.trace.TraceList;

import com.google.gson.JsonArray;
import ru.meow.quickseis.filter.algorithm.FFT;
import ru.meow.quickseis.util.ComplexDigit;


public class FrequencyRejectionFilter extends BaseFilter implements Filter
{
    
    //Value of frequency which should be rejected
    private int frequency;
    
    //Width of frequency interval which will be rejected (frequency+/-bandwith/2)
    private int bandwith;
    
    //First trace for filter applying
    private int startX;
    
    //Last trace for filter applying
    private int finishX;
    
    public FrequencyRejectionFilter (String sortingTracesFieldParameterName, JsonArray arguments)
    {
        super(sortingTracesFieldParameterName);
        this.frequency = arguments.get(0).getAsInt();
        this.bandwith = arguments.get(1).getAsInt();
        this.startX = arguments.get(2).getAsInt();
        this.finishX = arguments.get(3).getAsInt();
    }
    
    @Override
    public void apply (TraceList traces)
    {
        traces.sort(sortingTracesFieldName);
        
        for (Trace trace : traces)
        {
            ComplexDigit[] fourierCoefficients = FFT.getFourierCoefficientsFromPoints(trace.getPoints());
            fourierCoefficients = RejectFrequency(fourierCoefficients, frequency, bandwith);
            FFT.updatePointsFromFourierCoefficients(fourierCoefficients, trace.getPoints());
        }
    }
    
    private ComplexDigit[] RejectFrequency (ComplexDigit[] points, int frequency, int bandwith)
    {
        int n = points.length;
        for (int i = frequency - bandwith / 2; i <= frequency + bandwith / 2; i++)
        {
            if (i >= 0 && i <= n)
            {
                points[i].re = 0;
                points[i].im = 0;
                points[n - i].re = 0;
                points[n - i].im = 0;
            }
        }
        
        return points;
    }
    
    
    
}
