package ru.meow.quickseis.filter;

import com.google.gson.JsonArray;
import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.entity.trace.TraceList;
import ru.meow.quickseis.filter.algorithm.FFT;
import ru.meow.quickseis.util.ComplexDigit;

public class BandpassFilter extends BaseFilter implements Filter
{
    private int minFrequency;
    private int maxFrequency;
    
    public BandpassFilter (String sortingTracesFieldParameterName, JsonArray arguments)
    {
        super(sortingTracesFieldParameterName);
        this.minFrequency = arguments.get(0).getAsInt();
        this.maxFrequency = arguments.get(0).getAsInt();
    }
    
    @Override
    public void apply (TraceList traces)
    {
        traces.sort(sortingTracesFieldName);
    
        for (Trace trace : traces)
        {
            ComplexDigit[] fourierCoefficients = FFT.getFourierCoefficientsFromPoints(trace.getPoints());
            fourierCoefficients = RejectFrequency(fourierCoefficients, minFrequency, maxFrequency);
            FFT.updatePointsFromFourierCoefficients(fourierCoefficients, trace.getPoints());
        }
    }
    
    private ComplexDigit[] RejectFrequency (ComplexDigit[] points, int minFrequency, int maxFrequency)
    {
        int n = points.length;
        for (int i = 0; i < minFrequency; i++)
        {
            points[i].re = 0;
            points[i].im = 0;
            points[n - i].re = 0;
            points[n - i].im = 0;
        }
        
        for (int i = maxFrequency + 1; i <= n / 2; i++)
        {
            points[i].re = 0;
            points[i].im = 0;
            points[n - i].re = 0;
            points[n - i].im = 0;
        }
        
        return points;
    }
}
