package ru.meow.quickseis.filter;

import com.google.gson.JsonArray;
import ru.meow.quickseis.entity.trace.Point;
import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.entity.trace.TraceList;

import java.util.ArrayList;

public class ScaleFilter extends BaseFilter implements Filter
{
    private float scaleX;
    private float scaleT;
    
    public ScaleFilter (String sortingTracesFieldParameterName, JsonArray arguments)
    {
        super(sortingTracesFieldParameterName);
        this.scaleX = arguments.get(0).getAsFloat();
        this.scaleT = arguments.get(1).getAsFloat();
    }
    
    @Override
    public void apply (TraceList traces)
    {
        traces.sort(sortingTracesFieldName);
        
        for (Trace trace : traces)
        {
            ArrayList<Point> points = trace.getPoints();
            
            for (Point point: points)
            {
                point.setT(point.getT() * scaleT);
                point.setX(point.getX() * scaleX);
            }
        }
    }
}
