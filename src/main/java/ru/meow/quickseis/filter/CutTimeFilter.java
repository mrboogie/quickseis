package ru.meow.quickseis.filter;

import com.google.gson.JsonArray;
import ru.meow.quickseis.entity.trace.Point;
import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.entity.trace.TraceList;

import java.util.ArrayList;

public class CutTimeFilter extends BaseFilter implements Filter
{
    private float tMax;
    
    public CutTimeFilter (String sortingTracesFieldParameterName, JsonArray arguments)
    {
        super(sortingTracesFieldParameterName);
        this.tMax = arguments.get(0).getAsInt() / 1000f;
    }
    
    @Override
    public void apply (TraceList traces)
    {
        traces.sort(sortingTracesFieldName);
        
        for (Trace trace : traces)
        {
            ArrayList<Point> points = trace.getPoints();
            int beginRemoveIndex = getBeginRemoveIndex(points);
            for (int index = points.size() - 1; index >= beginRemoveIndex; index--)
            {
                points.remove(index);
            }
        }
    }
    
    public int getBeginRemoveIndex (ArrayList<Point> points)
    {
        int left = 0, right = points.size() - 2;
        
        while (true)
        {
            int mid = (left + right) / 2;
            
            if (tMax < points.get(mid).getT())
            {
                right = mid - 1;
            }
            if (tMax >= points.get(mid + 1).getT())
            {
                left = mid + 1;
            }
            if (tMax >= points.get(mid).getT() && tMax < points.get(mid + 1).getT())
            {
                return mid + 1;
            }
            
            if (left > right)
            {
                if (left == 0)
                {
                    return 0;
                }
                
                return points.size();
            }
        }
    }
}
