package ru.meow.quickseis.entity.trace;

import java.beans.PropertyDescriptor;
import java.util.Comparator;

public class TraceComparator implements Comparator<Trace>
{
    private PropertyDescriptor propertyDescriptor;
    
    public TraceComparator (String sortingFieldName)
    {
        try
        {
            this.propertyDescriptor = new PropertyDescriptor(sortingFieldName, Trace.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public int compare (Trace firstTrace, Trace secondTrace)
    {
        try
        {
            int firstTraceSortingFieldValue = (Integer)propertyDescriptor.getReadMethod().invoke(firstTrace);
            int secondTraceSortingFieldValue = (Integer)propertyDescriptor.getReadMethod().invoke(secondTrace);
            return Integer.compare(firstTraceSortingFieldValue, secondTraceSortingFieldValue);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return 0;
        }
    }
}
