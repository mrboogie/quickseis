package ru.meow.quickseis.entity.trace;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.meow.quickseis.exception.ServerException.handleException;

public class WebglTrace
{
    private List<Float> vertices = new ArrayList<Float>();
    private List<Integer> indices = new ArrayList<Integer>();
    private List<Float> color;
    private String primitive;
    
    private int sequenceSeismogramNumber;
    private int fieldSeismogramNumber;
    private int shotPointNumber;
    private int shotPoint;
    private int depthPoint;
    private int offsetPoint;
    private int offsetPointDistance;
    
    public WebglTrace (Trace trace)
    {
        this.sequenceSeismogramNumber = trace.getSequenceSeismogramNumber();
        this.fieldSeismogramNumber = trace.getFieldSeismogramNumber();
        this.shotPointNumber = trace.getShotPointNumber();
        this.shotPoint = trace.getShotPoint();
        this.depthPoint = trace.getDepthPoint();
        this.offsetPoint = trace.getOffsetPoint();
        this.offsetPointDistance = trace.getOffsetPointDistance();
        this.color = Arrays.asList(0.0f, 0.0f, 0.0f, 1.0f);
        this.primitive = "t";
        transformPoints(trace.getPoints());
    }
    
    private void transformPoints (ArrayList<Point> points)
    {
        ArrayList<Integer> triangleList = new ArrayList<>();
        int pointNumber = 0;
        float x, t = 0, xOld = 0, tOld = 0;
        
        for (int i = 0; i < points.size(); i++)
        {
            x = points.get(i).getX();
            t = points.get(i).getT();
            
            if (xOld == 0 && x > 0)
            {
                if (pointNumber == 0)
                {
                    vertices.add(0f);
                    vertices.add(t);
                    pointNumber++;
                }
                triangleList.add(pointNumber - 1);
                triangleList.add(pointNumber);
            }
            else if (xOld < 0 && x > 0)
            {
                vertices.add(0f);
                vertices.add((t * xOld - tOld * x) / (xOld - x));
                pointNumber++;
                triangleList.add(pointNumber - 1);
                triangleList.add(pointNumber);
            }
            else if (xOld > 0 && x > 0)
            {
                triangleList.add(pointNumber);
            }
            else if (xOld > 0 && x == 0)
            {
                triangleList.add(pointNumber);
                addIndices(triangleList);
                triangleList.clear();
            }
            else if (xOld > 0 && x < 0)
            {
                vertices.add(0f);
                vertices.add((t * xOld - tOld * x) / (xOld - x));
                triangleList.add(pointNumber);
                pointNumber++;
                addIndices(triangleList);
                triangleList.clear();
            }
            
            vertices.add(x);
            vertices.add(t);
            pointNumber++;
            xOld = x;
            tOld = t;
        }
        
        if (triangleList.size() != 0)
        {
            vertices.add(0f);
            vertices.add(t);
            triangleList.add(pointNumber);
            addIndices(triangleList);
            triangleList.clear();
        }
    }
    
    private void addIndices (ArrayList<Integer> triangleList)
    {
        for (int i = 1; i < triangleList.size() - 1; i++)
        {
            indices.add(triangleList.get(0));
            indices.add(triangleList.get(i));
            indices.add(triangleList.get(i + 1));
        }
    }
}
