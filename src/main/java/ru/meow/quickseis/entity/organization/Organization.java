package ru.meow.quickseis.entity.organization;

public class Organization
{
    public static final String ID_DB_INNER_LABEL = "id";
    public static final String NAME_DB_LABEL = "name";
    public static final String ID_DB_OUTER_LABEL = "organization_id";
    
    private int id;
    private String name;
}
