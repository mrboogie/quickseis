package ru.meow.quickseis.entity.fileobject;

public class Directory
{
    public static final String ID_DB_INNER_LABEL = "id";
    public static final String NAME_DB_LABEL = "name";
    public static final String PARENT_ID_DB_LABEL = "parent_id";
    public static final String ID_DB_OUTER_LABEL = "directory_id";
    public static final String FILE_OBJECT_TYPE = "directory";
    public static final String DIRECTORY_NAME_PARAMATER_NAME = "directory_name";
    public static final String PARENT_DIRECTORY_ID_PARAMATER_NAME = "parent_directory_id";
    
    private int id;
    private String name;
    private Integer parentId;
    
    public Directory (int id, String name, Integer parentId)
    {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }
    
    // Getters and setters
    
    public int getId ()
    {
        return id;
    }
    
    public String getName ()
    {
        return name;
    }
    
    public Integer getParentId ()
    {
        return parentId;
    }
    
}
